var product1 = new Product("p1", "product 1", 100, 5, 0);
var product2 = new Product("p2", "product 2", 200, 0, 3);
var product3 = new Product("p3", "product 3", 300, 10, 5);
var product4 = new Product("p4", "product 4", 150, 50, 0);
var product5 = new Product("p5", "product 5", 200, 0, 10);
var product6 = new Product("p6", "product 6", 140, 40, 8);

var products = [product1, product2, product3, product4, product5, product6];


function getShuffledUniqueImageNames(myArrayOfImages) {
    var copiesImages = pictures.slice(0);
    for (var index = 0; index < copiesImages.length; index++) {
        var randIndex = Math.floor(Math.random() * copiesImages.length);
        var tmpValue = copiesImages[index];
        copiesImages[index] = copiesImages[randIndex];
        copiesImages[randIndex] = tmpValue;
    }
    return copiesImages;
}


function productionTableWithParameters(arrayOfProducts, arrayOfPictureNames) {

    var myTable = "";
    myTable = '<table border = "2"><th>Product Number</th><th>Description</th><th>Price</th><th>Fee</th>' +
        '<th>Available Qty</th><th>Total Amount</th><th>Image</th>';
    var sumOfQuantity = 0;
    var sumOfFee = 0;
    for (var row = 1; row <= arrayOfProducts.length; row++) {
        myTable += "<tr>";
        myTable += "<td>" + arrayOfProducts[row - 1].productNumber + "</td>";
        myTable += "<td>" + arrayOfProducts[row - 1].description + "</td>";
        myTable += "<td>" + arrayOfProducts[row - 1].price + "</td>";
        if (arrayOfProducts[row - 1].fee == 0) {
            myTable += "<td bgcolor = red>" + arrayOfProducts[row - 1].fee + "</td>";
        } else {
            myTable += "<td>" + arrayOfProducts[row - 1].fee + "</td>";
        }
        if (arrayOfProducts[row - 1].quantity == 0) {
            myTable += "<td bgcolor = pink>" + arrayOfProducts[row - 1].quantity + "</td>";
        } else {
            myTable += "<td>" + arrayOfProducts[row - 1].quantity + "</td>";
        }
        myTable += "<td>" + arrayOfProducts[row - 1].totalAmt() + "</td>";
        myTable += '<td><img src = "images/' + arrayOfPictureNames[row - 1] +
            '"width = "50" height = "50" /></td>';
        sumOfQuantity += arrayOfProducts[row - 1].quantity;
        sumOfFee += arrayOfProducts[row - 1].fee;

    }

    myTable += "<tr><td></td><td></td><td></td><td>" + sumOfFee +
        "</td><td>" + sumOfQuantity + "</td><td></td><td></td></tr>";
    myTable += "</table>";
    return myTable;
}

document.getElementById("mytable").innerHTML =
    productionTableWithParameters(products, getShuffledUniqueImageNames(pictures));